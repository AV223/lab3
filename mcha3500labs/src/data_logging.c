#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <inttypes.h> // For PRIxx and SCNxx macros
#include "stm32f4xx_hal.h" // to import UNUSED() macro
#include "cmd_line_buffer.h"
#include "cmd_parser.h"
#include "cmsis_os2.h"

#include "pendulum.h"
#include "IMU.h"

/* Variable declarations */
 float logCount;

 /* Function declarations */
 static void log_pendulum(void *argument);
 static void logging_stop(void);
 static void (*log_function)(void);
 static void log_pointer(void *argument);
 static void log_imu(void *argument);

 
 /* Timer */
osTimerId_t     _timer_id;  
osTimerAttr_t   _timer_attr = 
{
    .name = "timer"
};
 

 /* Function defintions */
 static void log_pendulum(void *arguments)
{
 /* TODO: Supress compiler warnings for unused arguments */
//UNUSED(En);
 /* TODO: Read the potentiometer voltage */
 float V = pendulum_read_voltage();
 
 /* TODO: Print the sample time and potentiometer voltage to the serial terminal in the format [time],[voltage] */
float time  = logCount/200.0f;

printf("%f,%f\n", time, V);

 /* TODO: Increment log count */
 logCount = logCount + 1;

 /* TODO: Stop logging once 2 seconds is reached (Complete this once you have created the stop function
in the next step) */
if (time == 2.0)
{
	logging_stop();
}	


}

void logging_init(void)
{
 /* TODO: Initialise timer for use with pendulum data logging */
 _timer_id = osTimerNew(log_pointer, osTimerPeriodic, NULL, &_timer_attr);

}

void pend_logging_start(void)
{
	/* TODO: Change function pointer to the pendulum logging function */
	log_function = &log_pendulum;
 
	/* TODO: Reset the log counter */
	logCount = 0;

	/* TODO: Start data logging timer at 200Hz */
	osTimerStart(_timer_id, (5));
 
}

void logging_stop(void)
{
 /* TODO: Stop data logging timer */
 osTimerStop(_timer_id);
}

static void log_pointer(void *argument)
 {
	 UNUSED(argument);

	 /* Call function pointed to by log_function */
	 (*log_function)();
 }
 
 void imu_logging_start(void)
 {
	/* TODO: Change function pointer to the imu logging function (log_imu) */
	log_function = &log_imu;

	/* TODO: Reset the log counter */
	logCount = 0;

	/* TODO: Start data logging at 200Hz */
	osTimerStart(_timer_id, (5));
	
 }

 static void log_imu(void *arguments)
 {
 /* TODO: Read IMU */
	IMU_read();

	/* TODO: Get the imu angle from accelerometer readings */
	float acc_angle = get_acc_angle();

	/* TODO: Get the imu X gyro reading */
	float gyroX = get_gyroX();

	/* TODO: Read the potentiometer voltage */
	float V = pendulum_read_voltage();
	
	/*Calculate the time*/
	float time  = logCount/200.0f;

	/* TODO: Print the time, accelerometer angle, gyro angular velocity and pot voltage values to the serial terminal in the format %f,%f,%f,%f\n */
	printf("%f,%f,%f,%f\n", time, acc_angle, gyroX, V);
	
	/* TODO: Increment log count */
	logCount = logCount + 1;
	
	/* TODO: Stop logging once 5 seconds is reached */
	if (time == 2.0)
{
	logging_stop();
}
 }