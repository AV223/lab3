#ifndef IMU_H
#define IMU_H

 void IMU_init(void);
 void IMU_read(void);
 float get_accY(void);
 double get_acc_angle(void);
 float get_gyroX(void);

#endif