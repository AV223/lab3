#include "tm_stm32_mpu6050.h"
#include <stdint.h>
#include <math.h>
#include "cmsis_os2.h"
#include "uart.h"

#include "IMU.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
 #endif

/* Variable declarations */
TM_MPU6050_t IMU_datastruct;

/* Function defintions */
void IMU_init(void)
 {
	/* TODO: Initialise IMU with AD0 LOW, accelleration sensitivity +-4g, gyroscope +-250 deg/s */
	TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
 }
 
void IMU_read(void)
 {
	/* TODO: Read all IMU values */
	TM_MPU6050_ReadAll(&IMU_datastruct);
 }
 
 float get_accY(void)
 {
	/* TODO: Convert accelleration reading to ms^-2 */
	float Y_acc = IMU_datastruct.Accelerometer_Y/8192.0;
	Y_acc = Y_acc*9.81;
	
	/* TODO: return the Y acceleration */
	return Y_acc;
 }
 
 float get_accZ(void)
 {
	 /* TODO: Convert accelleration reading to ms^-2 */
	float Z_acc = IMU_datastruct.Accelerometer_Z/8192.0;
	Z_acc = Z_acc*9.81;
	
	/* TODO: return the Z acceleration */
	return Z_acc;
 }

 float get_gyroX(void)
 {
	 
	float X_gy = IMU_datastruct.Gyroscope_X/131.0f;
	X_gy = X_gy*(M_PI/180.0f);
	/* TODO: return the X angular velocity */
	return X_gy;
 }
 
 double get_acc_angle(void)
 {
	/* TODO: compute IMU angle using accY and accZ using atan2 */
	double acc_angle = -atan2(get_accZ(),get_accY());

	/* TODO: return the IMU angle */
	return acc_angle;
 }